package com.example.victor.profile_viewer.dagger;

import dagger.Component;

/**
 * Created by victor on 10.10.16.
 */

public class InjectHelper {

    private static RootComponent sRootComponent;

    static {
        initModules();
    }

    private static void initModules() {
        //sRootComponent = getRootComponentBuilder().build();
    }

   /* public static DaggerRootComponent.Builder getRootComponentBuilder() {
        return DaggerRootComponent.builder();
    }*/

    public static RootComponent getRootComponent() {
        if (sRootComponent == null) {
            initModules();
        }
        return sRootComponent;
    }
}
