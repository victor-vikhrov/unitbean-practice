package com.example.victor.profile_viewer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.victor.profile_viewer.api.ProfileService;
import com.example.victor.profile_viewer.model.Album;
import com.example.victor.profile_viewer.model.Photo;
import com.example.victor.profile_viewer.model.UserProfile;
import com.example.victor.profile_viewer.view.AlbumListAdapter;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProfileActivity extends AppCompatActivity {

    public static final String USER_ID = "user_id";
    public static final String USER = "user";


    private final String baseUrl = "https://jsonplaceholder.typicode.com";

    private int currentUserId;

    private ProfileService service;

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private AlbumListAdapter adapter;
    private UserProfile currentUser;

    private List<Album> userAlbums;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initService();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_albums_list);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));


        initToolbar();

        currentUserId = getIntent().getExtras().getInt(USER_ID);
        Gson gson = new Gson();
        currentUser = gson.fromJson(getIntent().getExtras().getString(USER), UserProfile.class);
        refreshUserProfile();


        //getUserById(currentUserId);
        getUserAlbums(currentUserId);

    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void getAlbumPhotos(Album album, final int position) {
        Call<List<Photo>> call = service.fetchPhotosByAlbumId(album.getId());
        call.enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
                if (response.isSuccessful()) {
                    List<Photo> photos = response.body();

                        Log.d("MYLOG", photos.get(0).getAlbumId() + " : " + " photo : " +
                                photos.get(0).getId() + " url : "+ photos.get(0).getUrl());

                    userAlbums.get(position).attach(photos);
                    adapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onFailure(Call<List<Photo>> call, Throwable t) {

            }
        });
    }

    private void getUserAlbums(int currentUserId) {
        Call<List<Album>> call = service.fetchAlbumsByUserId(currentUserId);
        call.enqueue(new Callback<List<Album>>() {
            @Override
            public void onResponse(Call<List<Album>> call, Response<List<Album>> response) {
                if (response.isSuccessful()) {
                    List<Album> albums = response.body();
                    userAlbums = albums;
                    int i = 0;
                    for (Album album : userAlbums) {
                        Log.d("MYLOG", "album : " + album.getId());
                        getAlbumPhotos(album, i++);
                    }
                    refreshAlbums(userAlbums);
                }
            }
            @Override
            public void onFailure(Call<List<Album>> call, Throwable t) {

            }
        });
    }

    private void initService() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(ProfileService.class);
    }

    private void getUserById(int currentUserId) {
        Call<UserProfile> call = service.fetchUserById(currentUserId);
        call.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(Call<UserProfile> call, Response<UserProfile> response) {
                if (response.isSuccessful()) {
                    UserProfile currentUser = response.body();
                    //refreshUserProfile();
                }
            }
            @Override
            public void onFailure(Call<UserProfile> call, Throwable t) {

            }
        });
    }

    private void refreshUserProfile() {

        if (currentUser != null) {
            getSupportActionBar().setTitle(currentUser.getName());
            TextView tNickname = (TextView) findViewById(R.id.t_profile_title);
            tNickname.setText(currentUser.getUsername());
        }


    }

    private void refreshAlbums(List<Album> albums) {
        if (adapter != null) {
            adapter.refreshList(albums);
            adapter.notifyDataSetChanged();
        } else {
            adapter = new AlbumListAdapter(this, recyclerView, albums);
            recyclerView.setAdapter(adapter);
        }

        recyclerView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


}
