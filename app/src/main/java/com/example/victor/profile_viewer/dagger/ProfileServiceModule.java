package com.example.victor.profile_viewer.dagger;

import com.example.victor.profile_viewer.api.ProfileService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by victor on 10.10.16.
 */

@Module
public class ProfileServiceModule {

    @Provides
    @Singleton
    public ProfileService providesProfileService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(ProfileService.class);
    }

}
