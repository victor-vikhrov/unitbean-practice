package com.example.victor.profile_viewer.view;

import android.content.Context;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.victor.profile_viewer.R;
import com.example.victor.profile_viewer.model.Album;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import okhttp3.OkHttpClient;
import java.util.List;

/**
 * Created by victor on 11.10.16.
 */

public class AlbumListAdapter extends RecyclerView.Adapter {

    private final RecyclerView recyclerView;

    private Context context;
    private List<Album> albums;

    public void refreshList(List<Album> albums) {
        this.albums = albums;
    }


    class AlbumViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        int albumId;
        int userId;
        String url;
        boolean imageIsLoaded;
        ImageView imPhoto;
        TextView tAlbumTitle, tPhotoTitle;

        public AlbumViewHolder(View itemView) {
            super(itemView);

            imageIsLoaded = false;
            imPhoto = (ImageView) itemView.findViewById(R.id.im_album_item);
            tAlbumTitle = (TextView) itemView.findViewById(R.id.t_album_item_title);
            tPhotoTitle = (TextView) itemView.findViewById(R.id.t_album_item_description);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            Snackbar snackbar = Snackbar
                    .make(recyclerView, "Album #" + albumId, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }


    public AlbumListAdapter(Context context, RecyclerView recyclerView, List<Album> albums) {
        this.albums = albums;
        this.context = context;
        this.recyclerView = recyclerView;
        for (Album al : albums) {
            Log.d("MYLOG", al.toString());
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_list_item, parent, false);
        AlbumViewHolder view = new AlbumViewHolder(v);
        return view;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final AlbumViewHolder view = (AlbumViewHolder) holder;
        //view.imPhoto.setBackgroundResource(R.drawable.v_photo_not_loaded);
        view.tAlbumTitle.setText(albums.get(position).getTitle());
        view.albumId = (int) albums.get(position).getId();
        view.userId = (int) albums.get(position).getUserId();
        if (albums.get(position).getPhotos() != null && albums.get(position).getPhotos().size() > 0) {
            String url = albums.get(position).getPhotos().get(0).getThumbnailUrl();
            view.url = url.replaceFirst("^(http://www\\.|http://|www\\.)","https://");
            Picasso.with(context).load(view.url).into(view.imPhoto);
        }

    }

    @Override
    public int getItemCount() {
        return albums.size();
    }
}
