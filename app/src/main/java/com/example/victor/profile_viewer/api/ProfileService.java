package com.example.victor.profile_viewer.api;

import com.example.victor.profile_viewer.model.Album;
import com.example.victor.profile_viewer.model.Photo;
import com.example.victor.profile_viewer.model.UserProfile;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by victor on 09.10.16.
 */

public interface ProfileService {

    @GET("/users")
    Call<List<UserProfile>> fetchUsers();

    @GET("/users/{id}")
    Call<UserProfile> fetchUserById(@Path("id") int id);

    @GET("/albums")
    Call<List<Album>> fetchAlbumsByUserId(@Query("userId") long id);

    //https://jsonplaceholder.typicode.com/photos?albumId=1
    @GET("photos")
    Call<List<Photo>> fetchPhotosByAlbumId(@Query("albumId") long albumId);

    @GET("/150/39e985")
    Call<Object> testRequest();
}
