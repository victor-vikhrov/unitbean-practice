package com.example.victor.profile_viewer.dagger;

import com.example.victor.profile_viewer.UserListActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by victor on 10.10.16.
 */

@Singleton
@Component(modules = {
        ProfileServiceModule.class
})

public interface RootComponent {
    void inject(UserListActivity activity);
}
