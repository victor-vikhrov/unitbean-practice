package com.example.victor.profile_viewer.callback;

import android.view.View;

/**
 * Created by victor on 11.10.16.
 */

public interface OnRecyclerItemClickListener {
    public void onRecyclerItemClick(View v, int position);
}
