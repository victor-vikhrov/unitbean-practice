package com.example.victor.profile_viewer.model;

/**
 * Created by victor on 09.10.16.
 */

public class Photo {

    public long getAlbumId() {
        return albumId;
    }

    public String getTitle() {
        return title;
    }

    public long getId() {
        return id;
    }

    private long albumId;
    private long id;
    private String title;
    private String url;
    private String thumbnailUrl;


    @Override
    public String toString(){
        return albumId + "  " + " title : " + title + " url : " + url;
    }

    public String getUrl() {
        return url;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }
}
