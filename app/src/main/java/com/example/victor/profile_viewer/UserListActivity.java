package com.example.victor.profile_viewer;

import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.victor.profile_viewer.api.ProfileService;
import com.example.victor.profile_viewer.callback.OnRecyclerItemClickListener;
import com.example.victor.profile_viewer.model.Photo;
import com.example.victor.profile_viewer.model.UserProfile;
import com.example.victor.profile_viewer.view.UserListAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserListActivity extends AppCompatActivity implements OnRecyclerItemClickListener {

    private static String LOG_TAG = "MainActivity";
    private final String baseUrl = "https://jsonplaceholder.typicode.com";
    private List<UserProfile> users;

    private ImageView photoView;
    private Object request;

    private RecyclerView recyclerView;
    private UserListAdapter adapter;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false));
        getUsers();

        //getAlbumTitle();


    }

    private void refreshUsers(List<UserProfile> users) {

        if (adapter != null) {
            // refresh adapter data
        } else {
            adapter = new UserListAdapter(this, users);
            recyclerView.setAdapter(adapter);
        }
        recyclerView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);



    }




    private void getUsers() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(baseUrl)
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
        ProfileService service = retrofit.create(ProfileService.class);
        Call<List<UserProfile>> call = service.fetchUsers();
        call.enqueue(new Callback<List<UserProfile>>() {
            @Override
            public void onResponse(Call<List<UserProfile>> call, Response<List<UserProfile>> response) {
                if (response.isSuccessful()) {
                    List<UserProfile> users = response.body();
                    refreshUsers(users);
                }
            }

            @Override
            public void onFailure(Call<List<UserProfile>> call, Throwable t) {
                Log.d(LOG_TAG, "failure");
            }


        });
    }


    public void getRequest() {


    }

    @Override
    public void onRecyclerItemClick(View v, int position) {

    }
}
