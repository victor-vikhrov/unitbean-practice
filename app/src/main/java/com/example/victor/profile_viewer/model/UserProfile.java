package com.example.victor.profile_viewer.model;

import org.json.JSONObject;

/**
 * Created by victor on 09.10.16.
 */

public class UserProfile {

    private int id;
    private String name;
    private String username;
    private String email;
    private Address address;
    private String phone;
    private String website;
    private Company company;


    public UserProfile(String name, String nick) {
        this.username = nick;
        this.name = name;
    }

    @Override
    public String toString(){

        return id + " " +  name + " " + email;
    }


    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
