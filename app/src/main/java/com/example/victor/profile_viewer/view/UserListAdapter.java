package com.example.victor.profile_viewer.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.victor.profile_viewer.ProfileActivity;
import com.example.victor.profile_viewer.R;
import com.example.victor.profile_viewer.model.UserProfile;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by victor on 10.10.16.
 */

public class UserListAdapter extends RecyclerView.Adapter {

    class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView nickname, name;
        int id;

        public UserViewHolder(View itemView) {
            super(itemView);
            nickname = (TextView) itemView.findViewById(R.id.t_user_nick);
            name = (TextView) itemView.findViewById(R.id.t_user_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, ProfileActivity.class);
            intent.putExtra(ProfileActivity.USER_ID, this.id);
            Gson gson = new Gson();
            intent.putExtra(ProfileActivity.USER, gson.toJson(users.get(this.getPosition())));
            context.startActivity(intent);
        }
    }

    private List<UserProfile> users;
    private Context context;

    public UserListAdapter(Context context, List<UserProfile> users) {
        this.users = users;
        this.context = context;
        for (UserProfile p : users) {
            //Log.d("MYLOG", p.getUsername());
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item, parent, false);
        UserViewHolder view = new UserViewHolder(v);
        return view;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        UserViewHolder view = (UserViewHolder) holder;
        view.nickname.setText(users.get(position).getName());
        view.name.setText(users.get(position).getUsername());
        view.id = users.get(position).getId();
        Log.d("MYLOG", users.get(position).getName() + "  " + users.get(position).getUsername());

    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
