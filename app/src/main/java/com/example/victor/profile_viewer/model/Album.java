package com.example.victor.profile_viewer.model;

import java.util.List;

/**
 * Created by victor on 09.10.16.
 */

public class Album {

    private int userId;
    private int id;

    public String getTitle() {
        return title;
    }

    public int getUserId() {
        return userId;
    }

    public int getId() {
        return id;
    }

    private String title;

    public List<Photo> getPhotos() {
        return photos;
    }

    List<Photo> photos;

    @Override
    public String toString(){
        return userId + " album : " + id + " title : " + title;
    }

    public void attach(List<Photo> photos) {
        this.photos = photos;
    }
}
