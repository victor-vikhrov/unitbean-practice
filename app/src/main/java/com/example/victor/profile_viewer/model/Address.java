package com.example.victor.profile_viewer.model;

/**
 * Created by victor on 09.10.16.
 */
public class Address {

    private String street;
    private  String suite;
    private String city;
    private String zipcode;
    private Geo geo;

    private class Geo {
        double lat;
        double lon;
    }
}
